# MixMode GCP FlowLog / Cloud Audit Monitor Sensor

## Steps

### Create a new Google Cloud Function

- Enter Cloud Functions
- Click on `Create Function`

#### Fill in the 'Basics'

1. Provide a unique `Function name`
2. Select the appropriate `Region` for this function
3. Select the `Cloud Pub/Sub` Trigger and select the `CREATE A TOPIC` from the drop down.
4. Provide a unique `Topic ID`.  I.e. `gpcLogsToMixMode`, and click `Create Topic`  # NOTE - Use Google managed encryption key for this step
5. Enable `Retry on Failure`
6. Click `VARIABLES, NETWORKING AND ADVANCED SETTINGS`
7. Click `Save`

#### Apply MixMode environment parameters

- Click `ENVIRONMENT VARIABLES`

For each provided parameter:

1. Click `+ ADD VARIABLE`
2. Paste in the parameter Name and Value
3. Click `Ok`

##### Example Values From MixMode

| Name | Value | Origin |
| ----------- | ----------- | ----------- |
| MM_ACCESS_KEY_ID | KEY_ID | From MixMode Customer Success |
| MM_SECRET_ACCESS_KEY | SECRET_KEY | From MixMode Customer Success |
| MM_REGION | us-west-2 | From MixMode Customer Success |
| MM_PROVIDER_ID | 1999 | From MixMode Customer Success |
| MM_TENANT_ID_FLOW_LOG | 2 | From MixMode UI - Flow Log Sensor Info Page |
| MM_SENSOR_ID_FLOW_LOG | 174 | From MixMode UI - Flow Log Sensor Info Page |
| MM_TENANT_ID_CLOUD_AUDIT_LOG | 2 | From MixMode UI - Cloud Audit Log Sensor Info Page |
| MM_SENSOR_ID_CLOUD_AUDIT_LOG | 175 | From MixMode UI - Cloud Audit Log Sensor Info Page |

##### Parameters from Google Cloud Platform

| Name | Value | Origin |
| ----------- | ----------- | ----------- |
| GCP_NETWORK_SUBNETS | 172.90.0.0/16,192.168.1.0/24 | Google Cloud Platform -> VPC Network -> VPC Networks -> IP address range |
| GCP_NETWORK_INTERFACES | 10.0.0.4/52.158.251.27,10.0.0.3/52.158.251.2 | Google Cloud Platform -> Compute Engine -> VM Instances |

NOTE: The SYNTAX for `GCP_NETWORK_INTERFACES` is `Internal_IP/External_IP`.  This is used to map the internal IP address from the flow log information to report the external IP address.

#### Apply source code to Function

- After configuring the `ENVIRONMENT VARIABLES`, click `NEXT`.
- Ensure the `Runtime` dropdown is set to `Node.js 10`
- Ensure the `Entry point` is set to `logsToMixMode`
- Select `index.js`, then paste in the text from the `index.js` file provided.
- Select `package.json`, then paste in the text from the `package.json` file provided.
- Click `DEPLOY`

### Configure Google Cloud Platform Traffic Flow Logs

- Enter Google Cloud Platform -> VPC Network -> VPC Networks
- Select the `subnet` to enable flow logs
- Click on `Edit`
- Select `On` under FlowLogs
- Click on `CONFIGURE_LOGS` drop down

1. Select the appropriate `Aggregation Interval`.  I.e. `5 SEC`
2. Select the appropriate `Sample Rate`.  I.e. `50%`
3. Click `SAVE`

![Enable](./EnableFlowLog.png)

- Click `View flow logs`
- If in the new Logs Viewer interface, click `Actions` -> `Create Sink`
- Click `Create Sink`
- Provide a unique name in the `Sink Name` input.  I.e. `gpcFlowLogs`.
- From the `Sink Service` drop down, select `Pub/Sub`
- From the `Sink Destination` drop down, the destination created by the function.  I.e. `gpcLogsToMixMode`
- Click `CLOSE` when prompted.

### Configure Google Cloud Platform Cloud Audit Logs

- Enter Google Cloud Platform -> Logging -> Logging Viewer
- Create a log query to report all log names.  Example:

```SQL
logName=("projects/<projectName>/logs/cloudaudit.googleapis.com%2Factivity" OR
"projects/<projectName>/logs/cloudaudit.googleapis.com%2Fdata_access" OR
"projects/<projectName>/logs/cloudaudit.googleapis.com%2Fsystem_event" OR
"projects/<projectName>/logs/clouderrorreporting.googleapis.com%2Finsights" OR
"projects/<projectName>/logs/GCEGuestAgent" OR
"projects/<projectName>/logs/GCEMetadataScripts" OR
"projects/<projectName>/logs/OSConfigAgent")
```

NOTE:  It is not recommended to include the logs from this Cloud Function.  This could cause an over abundance of log entries.

- If in the new Logs Viewer interface, click `Actions` -> `Create Sink`
- Click `Create Sink`
- Provide a unique name in the `Sink Name` input.  I.e. `gpcCloudAuditLogs`.
- From the `Sink Service` drop down, select `Pub/Sub`
- From the `Sink Destination` drop down, the destination created by the function.  I.e. `gpcLogsToMixMode`
- Click `Create Sink` when prompted.
- Click `Close`

At this point, your activity and flow logs should start flowing to mixmode.
