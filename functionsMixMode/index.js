////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MixMode.ai Google Cloud Flowlog / Audit Log Sensor 
////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Please customize the following variables, to match your environment.
// Your Provider ID, access key id, and secret access key, are assigned by MixMode and can be obtained from support.
// Your Tenant ID and Sensor ID should correspond to your created tenant and sensor in the MixMode UI. 

const providerId = process.env['MM_PROVIDER_ID'];
const tenantIdFlowLog = process.env['MM_TENANT_ID_FLOW_LOG'] || 0;
const sensorIdFlowLog = process.env['MM_SENSOR_ID_FLOW_LOG'] || 0;
const tenantIdCloudAuditLog = process.env['MM_TENANT_ID_CLOUD_AUDIT_LOG'] || 0;
const sensorIdCloudAuditLog = process.env['MM_SENSOR_ID_CLOUD_AUDIT_LOG'] || 0;
const accessKeyId = process.env['MM_ACCESS_KEY_ID'];
const secretAccessKey = process.env['MM_SECRET_ACCESS_KEY'];
const region = process.env['MM_REGION'];

// Subnets to report as being 'local'.  I.e 172.30.0.0/16,192.168.1.0/24
const subnet_cidrs = process.env['GCP_NETWORK_SUBNETS'].split(",");
// Network Interface private/public mapping.  I.e. 10.0.0.4/52.158.251.27,10.0.0.3/52.158.251.2
const nics = process.env['GCP_NETWORK_INTERFACES'].split(",");

// Local storage configuration
const localStorMax = process.env['LOCAL_STORE_MAX'] || 250;
const timeoutSeconds = process.env['SEND_TIMEOUT_SECONDS'] || 10;

////////////////////////////////////////////////////////////////////////////////////////////////////////////

// 500 records, 4 mb per putBatch request
// 5000 records per second, 5 megabytes per second

const maxRecordsPerBatch = 500;
const maxRecordsPerSecond = 5000;
const maxBytesPerBatch = 4 * 1024 * 1024;
const maxBytesPerSecond = 5 * 1024 * 1024;

const firehoseConfig = {
    region: region,
    credentials: {
        accessKeyId: accessKeyId,
        secretAccessKey: secretAccessKey
    }
};
console.info(`Env loaded.`);
const AWS  = require('aws-sdk');
console.info(`aws-sdk loaded.`);
const Firehose = new AWS.Firehose(firehoseConfig);
console.info(`firehose loaded.`);
const digits = '0123456789';
const CIDRMatcher = require('cidr-matcher');
console.info(`cidr-matcher loaded.`);
var matcher = new CIDRMatcher(subnet_cidrs);

var nic_map = [];
for (var nic in nics) {
    let parts = nics[nic].split('/');
    nic_map[parts[0]] = parts[1];
}
let recordStor = [];
let timeout;
exports.logsToMixMode = async function (event, context, callback) {
    const message = JSON.parse( Buffer.from(event.data, 'base64').toString());
    timeout = timeout || new Date().getTime() + timeoutSeconds*1000;
    var now = new Date().getTime();
    message.isflowLog = (message.logName.indexOf('vpc_flows') != -1);
    recordStor.push(message);
    console.info(`recordStore length - ${recordStor.length} - ${message.isflowLog} -  ${timeout} - ${(now - timeout)/1000}`);
    if(recordStor.length >= localStorMax || now >= timeout) {
        await processRecords();
        timeout = undefined;
        recordStor = [];
    }
    callback();
};

async function processRecords() {
    let totalRecords = 0;
    var zerofilled = ('00000000'+providerId).slice(-8);
    var firehoseName = `p${zerofilled}`;
    try {
        const toFirehose = [];
        let batchRecords = 0;
        let batchBytes = 0;
        let secondRecords = 0;
        let secondBytes = 0;
        // console.info(`num records - ${recordStor.length}`);
        for (const record of recordStor) {
            var rec = undefined;
            if (record.isflowLog) {
                rec = transformFlowLog(record);
            } else {
                rec = transformCloudAuditLog(record);
            }
            if (typeof rec === 'undefined') {
                continue;
            }
            const recStr = JSON.stringify(rec);
            const recLen = recStr.length;
            if (((secondRecords+1) > maxRecordsPerSecond) || ((secondBytes + recLen) > maxBytesPerSecond) || ((batchRecords+1) > maxRecordsPerBatch) || ((batchBytes + recLen) > maxBytesPerBatch)) {
                if (((secondRecords+1) > maxRecordsPerSecond) || ((secondBytes + recLen) > maxBytesPerSecond)) {
                    console.info("Sleeping 1 second due to Kinesis per-second limits");
                    await sleep(1000);
                    secondRecords = 0;
                    secondBytes = 0;
                }
                await Firehose.putRecordBatch({DeliveryStreamName: firehoseName, Records: toFirehose}).promise();
                // console.info(`Exported batch of ${toFirehose.length} records to MixMode platform`);
                batchRecords = 0;
                batchBytes = 0;
                toFirehose.length = 0;
            }
            toFirehose.push({ Data: recStr });
            batchRecords += 1;
            batchBytes += recLen;
            secondRecords += 1;
            secondBytes += recLen;
            totalRecords += 1;
        }
        if (toFirehose.length > 0) {
            await Firehose.putRecordBatch({DeliveryStreamName: firehoseName, Records: toFirehose}).promise();
            console.info(`Exported batch of ${toFirehose.length} records to MixMode platform`);
        }
    } catch (err) {
        console.info("Exception pushing to MixMode", err);
    }
    console.info(`Successfully exported ${totalRecords} total records to MixMode platform`);
};

function findPublic(ip) {
    return nic_map[ip] || ip;
}
function transformFlowLog(src) {
    if (tenantIdFlowLog === 0 || sensorIdFlowLog === 0) {
        // Flow Log Sensor not configured.
        return;
    }
    // console.debug(`fl src - ${JSON.stringify(src)}`);
    const dst = new Object();
    const flowlog = {
        'version': 1,
        'timestamp': src.timestamp,
        'srcAddr': src.jsonPayload.connection.src_ip,
        'dstAddr': src.jsonPayload.connection.dest_ip,
        'srcPort': parseInt(src.jsonPayload.connection.src_port, 10),
        'dstPort': parseInt(src.jsonPayload.connection.dest_port, 10),
        'protocol': src.jsonPayload.connection.protocol,
        'packets_out': parseInt(src.jsonPayload.packets_sent, 10),
        'packets_in': 0, // There does not appear to be a 'packets_in'
        'bytes_out': parseInt(src.jsonPayload.bytes_sent,10),
        'bytes_in': 0,// There does not appear to be a 'bytes_in'
        'start': new Date(src.jsonPayload.start_time).getTime(),
        'end': new Date(src.jsonPayload.end_time).getTime(),
        'logLine': JSON.stringify(src)
    };
    if (flowlog.packets_in === 0 && flowlog.packets_out === 0) {
        // filter out empty packets that are reported from flow log data
        return;
    }
    
    dst.providerId = providerId;
    dst.tenantId = tenantIdFlowLog;
    dst.sensorId = sensorIdFlowLog;
    dst.log = 'conn';
    dst.ts = flowlog.start/1000;
    dst.id_orig_h = findPublic(flowlog.srcAddr);
    dst.id_resp_h = findPublic(flowlog.dstAddr);
    if (!isNaN(flowlog.srcPort)) { dst.id_orig_p = flowlog.srcPort; }
    if (!isNaN(flowlog.dstPort)) { dst.id_resp_p = flowlog.dstPort; }
    dst.is_orig = true;
    dst.id_src_geo = 'US'; // @TODO: should this value even be present?
    dst.local_orig = false;

    if (typeof matcher !== 'undefined' && matcher.contains(flowlog.srcAddr)) {
        dst.id_src_geo = 'lo';
        dst.local_orig = true;
    }
    dst.id_dest_geo = 'US'; // @TODO: should this value even be present?
    dst.local_resp = false;
    if (typeof matcher !== 'undefined' && matcher.contains(flowlog.dstAddr)) {
        dst.id_dest_geo = 'lo';
        dst.local_resp = true;
    }
    
    dst.history = 'd';
    dst.duration = 0.0001;
    dst.orig_bytes = 0;
    dst.resp_bytes = 0;
    if (!isNaN(flowlog.end)) { dst.duration = ((flowlog.end) - (flowlog.start))/1000; }
    if (!isNaN(flowlog.bytes_out)) {
        dst.orig_bytes = flowlog.bytes_out;
    }
    if (!isNaN(flowlog.bytes_in)) {
        dst.resp_bytes = flowlog.bytes_in;
    }
    dst.flowlog = flowlog;
    // console.debug(`fl dst - ${JSON.stringify(dst)}`);
    return dst;
}

function transformCloudAuditLog(src) {
    if (tenantIdActivityLog === 0 || sensorIdActivityLog === 0) {
        // Activity Log Sensor not configured.
        return;
    }
    // console.debug(`al src - ${JSON.stringify(src)}`);
    if (typeof src.protoPayload.requestMetadata === 'undefined') return;
    if (typeof src.protoPayload.requestMetadata.callerIp === 'undefined') return;
    if (typeof src.protoPayload.methodName === 'undefined') return;
    const src_ip = src.protoPayload.requestMetadata.callerIp;
    const dst_ip = src.protoPayload.methodName;
    const dst = new Object();
    dst.providerId = providerId;
    dst.tenantId = tenantIdCloudAuditLog;
    dst.sensorId = sensorIdCloudAuditLog;
    dst.log = 'conn';
    dst.ts = new Date(src.timestamp).getTime()/1000;
    dst.id_orig_h = src_ip;
    dst.id_resp_h = dst_ip;
    dst.id_src_geo = 'US';
    dst.id_dest_geo = 'lo';
    dst.is_orig = true;
    dst.history = 'd';
    dst.local_orig = false;
    dst.local_resp = true;
    dst.duration = 1;
    dst.orig_bytes = 1;
    dst.resp_bytes = 1;
    dst.cloudtrail = src;
    // Google Cloud Logs do not appear to provide a method to determine if the traffic is
    // 'local' or 'outbound'.  Will need to revisit at some time in the future.
    // // Make the directionality "local" if the source IP address doesn't begin with a digit
    // const firstSourceAddressChar = src_ip.charAt(0);
    // if (digits.indexOf(firstSourceAddressChar) === -1) {
    //     dst.id_src_geo = 'lo';
    //     dst.id_dest_geo = 'lo';
    //     dst.local_orig = true;
    //     dst.local_resp = true;
    // }
    // // Flip the directionality to "Outbound" if the event name contains "LIST"
    // else if (src.operationName.indexOf('LIST') > -1) {
    //     dst.id_src_geo = 'lo';
    //     dst.id_dest_geo = 'US';
    //     dst.local_orig = true;
    //     dst.local_resp = false;
    //     dst.id_resp_h = src_ip;
    //     dst.id_orig_h = dst_ip;
    // }

    // console.debug(`al dst - ${JSON.stringify(dst)}`);
    return dst;
}

async function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}
