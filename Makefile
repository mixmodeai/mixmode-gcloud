IN_FILES = README.md functionsMixMode/index.js functionsMixMode/package.json 
ZIP_FILE=mixmode-gcloud.zip

all: $(ZIP_FILE)
	@echo Completed $@

$(ZIP_FILE):
	zip -q $@ $(IN_FILES)

clean:
	rm -f $(ZIP_FILE)
